* Synchronize CSV to letsfreckle.com

Pushes time entries from a CSV to letsfreckle.com

This is useful for people who work with different kinds of trackers
(Emacs Orgmode, Google Calendar, Toggl, etc), but want their time also
resembled in letsfreckle.

Note: This is a very simple one-way sync!

** Installation

=bundle install=

*** Configuration

=cp config_sample.yml config.yml=

**** TODO Create an API token

[[file:doc/freckle_api_token.png]]

**** TODO Obtain your user_id and project_id

  #+BEGIN_SRC restclient
  # Get User Info
  GET https://api.letsfreckle.com/v2/current_user
  Content-Type: application/json
  X-FreckleToken: [YOUR-TOKEN]

  # Get Project Info
  GET https://api.letsfreckle.com/v2/projects
  Content-Type: application/json
  X-FreckleToken: [YOUR-TOKEN]

  #+END_SRC

**** TODO Configure your freckle_token, user_id and project_id in config.yml
**** TODO Configure in =config.yml= from which date (=update_date=) you want to sync entries from the CSV to Freckle

** Usage

*** Example run

 #+BEGIN_EXAMPLE
   ~/src/200ok/csv2letsfreckle $ ./csv2letsfreckle.rb
   Updating project 'swisscom_tv'
   Found 1 legacy entries which will be deleted now.

   Deleting id: 22124826

   Creating new time entries.
   Create Entry '2019-01-28: #meeting Weekly Meeting / 20min.'
 #+END_EXAMPLE

** Timesheet CSV Format
   
The shared CSV format is as follows:

#+BEGIN_EXAMPLE
date,duration,hashtags,description
2018-05-28,4.5,#dev,cache invalidation
2018-05-28,0.25,#support #naming,helped naming things
#+END_EXAMPLE

The first four columns have to be =date=, =duration=, =hashtags=, and
=description=. Additional columns will be ignored, and thus can be used
to your liking.

**** Description of the columns

_date_: The date in ISO format YYYY-MM-DD.

_duration_: The duration in hours as a decimal number, e.g. =4.5= for
four and a half hours, =0.25= for 15 minutes.

_hashtags_: A =space= separated list of hashtags.

_description_: A short description on what you have worked on.

** More manual API tests

*** Create a new entry with =restclient-mode=

#+BEGIN_SRC restclient
  # Create new Entry
  POST https://api.letsfreckle.com/v2/entries
  Content-Type: application/json
  X-FreckleToken: [YOUR-TOKEN]
  {
  "date": "2018-05-22",
  "user_id": [YOUR-USER-ID]
  "minutes": 120,
  "description": "foobar baz",
  "project_id": [YOUR-PROJECT-ID]
  }

  # Get entries
  GET https://api.letsfreckle.com/v2/entries?from=2018-10-20&to=2018-11-01&project_ids=472403
  Content-Type: application/json
  X-FreckleToken: qjurnczgbwegzicntptgqiwjv0avn47-hioiyfa88nfoi68q7gy478k9b9e8l5i
#+END_SRC

** Rationale of using Ruby

   Letsfreckle has an API rate limit of 2 per second. Using a language
   which blocks by default and that can sleep actually gets this
   particular job done nicely.

   Also, with =inf-ruby= mode in Emacs, it's fun to do REPL-driven
   development!

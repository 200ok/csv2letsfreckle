require 'yaml'

class Config
  attr_accessor :config
  def initialize
    @config = YAML.load_file('config.yml')
  end
end

#!/usr/bin/env ruby

require 'csv'
require 'securerandom'
require 'rest-client'
require 'json'

require './config'

# Change into the current directory
Dir.chdir File.dirname(__FILE__)

@config = Config.new.config

def retrieve_entries(project_config)
  entries = RestClient.get('https://api.letsfreckle.com/v2/entries',
                           params: {
                             from: project_config['update_date'],
                             project_ids: project_config['project_id']
                           },
                           'Content-Type' => 'application/json',
                           'X-FreckleToken' => @config['freckle_token'])

  JSON.parse entries
end

def rate_limit
  # Rate limiting allows only 2 reqs per second
  sleep 0.5
end

def create_time_entries_in_freckle(project_config)
  puts "\n\nCreating new time entries.\n"

  CSV.foreach(project_config['timesheet_path'], { headers: true }) do |row|
    date            = row[0]
    minutes         = (row[1].to_f * 60).round(0)
    hashtags        = row[2]
    tmp_description = row[3]
    description     = "#{hashtags} #{tmp_description}"
    debouncer       = SecureRandom.uuid.split('-').first

    # A CSV file may contain rows which are older than the requested
    # `update_date`. The main rationale is that those times have been
    # synced to Freckle before.
    next if DateTime.strptime(date, '%Y-%m-%d') < project_config['update_date']

    payload = { 'user_id' => project_config['user_id'],
                'project_id' => project_config['project_id'],
                'date' => date,
                'minutes' => minutes,
                'description' => "#{description}, api debouncer #{debouncer}" }

    puts "Create Entry '#{date}: #{description} / #{minutes}min.'"

    res = RestClient.post('https://api.letsfreckle.com/v2/entries',
                          payload.to_json,
                          { 'Content-Type' => 'application/json',
                            'X-FreckleToken' => @config['freckle_token'] })

    puts "ERROR: #{res}" unless res.code == 201

    rate_limit
  end
end

def delete_time_entries_in_freckle(config)
  entries = retrieve_entries(config)
  while entries.count.positive?

    puts "Found #{entries.count} legacy entries which will be deleted now.\n\n"
    entries.map do |entry|
      entry_id = entry['id']

      puts "Deleting id: #{entry_id}"
      RestClient.delete("https://api.letsfreckle.com/v2/entries/#{entry_id}",
                        { 'Content-Type' => 'application/json',
                          'X-FreckleToken' => @config['freckle_token'] })
      rate_limit
    end

    entries = retrieve_entries(config)
  end
end

# MAIN

@config['projects'].each do |name, config|
  puts "Updating project '#{name}'"
  puts "--------------------------\n\n"

  delete_time_entries_in_freckle(config)

  create_time_entries_in_freckle(config)
end
